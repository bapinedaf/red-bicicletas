var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta')

describe('testing de bicicletas', function () {
  beforeEach(function (done) {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
      console.log('We are connected to test database');
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe('Bicicleta.createInstance', () => {
    it('Crea una instancia de Bicicleta', () => {
      var bici = Bicicleta.createInstance(1, "verde", "BMX", [49.1846099, -75.5991287]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("BMX");
      expect(bici.ubicacion[0]).toEqual(49.1846099);
      expect(bici.ubicacion[1]).toEqual(-75.5991287);
    })
  });

  describe('Bicicletas.allBicis', () => {
    it('Comienza vacía', (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicletas.add', () => {
    it('Agrega una sola bibicleta', (done) => {
      var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "Bmx" });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.find({}, function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toBe(aBici.code)
          done();
        });
      });
    });
  });

  describe('Bicicletas.findByCode', () => {
    it('Debe devolver la bici con code 1', (done) => {
      Bicicleta.find({}, (err, bicis) => {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({ code: 1, color: 'violeta', modelo: 'montaña' });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);

          var aBici2 = new Bicicleta({ code: 2, color: 'rojo', modelo: 'urbana' });
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(1, function (errr, targetBici) {
              expect(targetBici.code).toEqual(aBici.code);
              expect(targetBici.color).toEqual(aBici.color);
              expect(targetBici.modelo).toEqual(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe('Bicicletas.removeByCode', () => {
    it('Debe eliminar la bici con code 1', (done) => {
      Bicicleta.find({}, (err, bicis) => {
        expect(bicis.length).toBe(0);

        let aBici = new Bicicleta({ code: 1, color: 'violeta', modelo: 'montaña' });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);

          let aBici2 = new Bicicleta({ code: 2, color: 'rojo', modelo: 'urbana' });
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.removeByCode(1).
              then((targetBici) => {
                Bicicleta.find({})
                  .then((bicis) => {
                    expect(bicis.length).toBe(1);
                    done();
                  });
              }).catch((err) => {
                console.log(err);
              })
          });
        });
      });
    });
  });

});
/*
beforeEach(() => {Bicicleta.allBicis = []})

describe("Bicicleta.allBicis", () => {
  it("comienza vacia", function() {
    expect(Bicicleta.allBicis.length).toBe(0);
  })
})

describe('Bicicleta.add', () => {
  it("agregamos una", () => {
    expect(Bicicleta.allBicis.length).toBe(0)
    var a = new Bicicleta(1, 'rojo', 'urbana', [4.6718,-74.0638])
    Bicicleta.add(a)

    expect(Bicicleta.allBicis.length).toBe(1)
    expect(Bicicleta.allBicis[0]).toBe(a)
  })
})

describe("Bicicleta.findById", () => {
  it("debe buscar la bicicleta con el id correspondiente", function() {
    expect(Bicicleta.allBicis.length).toBe(0)
    var aBici1 = new Bicicleta(1, "verde", "urbana")
    var aBici2 = new Bicicleta(2, "rojo", "montaña")
    Bicicleta.add(aBici1)
    Bicicleta.add(aBici2)

    var targetBici = Bicicleta.findById(1)
    expect(targetBici.id).toBe(aBici1.id)
    expect(targetBici.color).toBe(aBici1.color)
    expect(targetBici.modelo).toBe(aBici1.modelo)
  })
})

describe("Bicicleta.removeById", () => {
  it("debe eliminar una bicicleta", function() {
    expect(Bicicleta.allBicis.length).toBe(0)
    var aBici1 = new Bicicleta(1, "verde", "urbana")
    var aBici2 = new Bicicleta(2, "rojo", "montaña")
    var aBici3 = new Bicicleta(3, "blanca", "ruta")
    Bicicleta.add(aBici1)
    Bicicleta.add(aBici2)
    Bicicleta.add(aBici3)

    expect(Bicicleta.allBicis.length).toBe(3)

    Bicicleta.removeById(1)
    expect(Bicicleta.allBicis.length).toBe(2)
  })
})
*/